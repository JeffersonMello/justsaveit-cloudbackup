$("#armazenamento").on('change', function(){calcJSI()});
$("#download").on('change', function(){calcJSI()});
$("#usuarios").on('change', function(){calcJSI()});
$("#monitoramento").change(function(){calcJSI()});




var calcJSI =  function() {
  var custStorage = 0,
  custDownload = 0,
  custMonitoramento = 0,
  custUsuarios = 0,
  total = 0;

  var custos = {
    licenseUser: 10.00,
    s3Storage:   0.97,
    s3Download:  0.65,
    monitor:     100.00
  };

  custStorage   = ( ($("#armazenamento").val()) * custos.s3Storage );
  custDownload  = ( ($("#download").val()) * custos.s3Download );

  if( $("#monitoramento").prop('checked') )
  custMonitoramento = custos.monitor;


  custUsuarios = ( ($("#usuarios").val()) * custos.licenseUser );


  total = ( custStorage + custDownload + custMonitoramento + custUsuarios );

  total = total.toFixed(2);

  $("#preco").text("R$ "+total);

}
