findString = function findText(text , e) {
  if (e.keyCode == 13) {
    window.find(text);
    $( "div:contains("+text+")" ).focus();
  }
}
/*========================================================================================================*/


function scrolldown(id){
  $('html, body').animate({
    scrollTop: $("#"+id).offset().top
  }, 1500);
}

function somentenum(valor, id){
	$("#"+id).val(valor.replace( /[^0-9\.]+/g, ""));
}

function hidePop(){
  if (screen.width < 600){
    $(".menu-flutuante").hide();
  } else {
    $(".menu-flutuante").show();
  }
}

//===============================================================================================================/

/* SLIDER */
jQuery(document).ready(function ($) {

  var jssor_1_options = {
    $AutoPlay: true,
    $SlideDuration: 800,
    $SlideEasing: $Jease$.$OutQuint,
    $ArrowNavigatorOptions: {
      $Class: $JssorArrowNavigator$
    },
    $BulletNavigatorOptions: {
      $Class: $JssorBulletNavigator$
    }
  };

  var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

  /*responsive code begin*/
  /*you can remove responsive code if you don't want the slider scales while window resizing*/
  function ScaleSlider() {
    var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
    if (refSize) {
      refSize = Math.min(refSize, 1920);
      jssor_1_slider.$ScaleWidth(refSize);
    }
    else {
      window.setTimeout(ScaleSlider, 30);
    }
  }
  ScaleSlider();
  $(window).bind("load", ScaleSlider);
  $(window).bind("resize", ScaleSlider);
  $(window).bind("orientationchange", ScaleSlider);
  /*responsive code end*/
});

var posFunc = $("#comofunciona").offset().top;
funcTop = posFunc+5;

//menu fixed
$(window).scroll(function(){
  var posScroll = $(this).scrollTop(),
  posMenu = $(".menu-flutuante").offset().top;

  if(posScroll >= posFunc) {
    $(".menu-flutuante").addClass("fixed");
  }else{
    $(".menu-flutuante").removeClass("fixed");
  }
});



// Range

var rangeSlider = function(){
  var slider = $('.range-slider'),
      range = $('.range-slider__range'),
      value = $('.range-slider__value');

  slider.each(function(){

    value.each(function(){
      var value = $(this).prev().attr('value');
      $(this).html(value);
    });

    range.on('input', function(){
      $(this).next(value).html(this.value);
    });
  });
};

rangeSlider();
