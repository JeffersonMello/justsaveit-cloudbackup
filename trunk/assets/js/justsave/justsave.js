if (screen.width <= 600) {
  var slider  = $("#slider"),
      image   = $(".imageselector");

  slider.removeClass('slider');
  slider.addClass('sliderMobile');

  image.removeClass('slide-image');
  image.addClass('slide-imageMobile');
} else {
  slider.removeClass('sliderMobile');
  slider.addClass('slider');

  image.removeClass('slide-imageMobile');
  image.addClass('slide-image');
}


findString = function findText(text , e) {
  if (e.keyCode == 13) {
    window.find(text);
    $( "div:contains("+text+")" ).focus();
  }
}
/*========================================================================================================*/


function scrolldown(id){
  $('html, body').animate({
    scrollTop: $("#"+id).offset().top
  }, 1500);
}
